.PHONY: edit data clean kernels-list kernels-push requirements requirements-plotly requirements-poetry test-environment

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')
PROFILE = default
PROJECT_NAME = kaggle-readability
PYTHON_INTERPRETER = poetry run python3

KAGGLE_USER = matthewfranglen
KAGGLE_COMPETITION = mlb-player-digital-engagement-forecasting
KERNELS = $(wildcard kaggle/*/*/kernel-metadata.json)
PUSH_KERNELS = $(KERNELS:%/kernel-metadata.json=.make/%)

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Run Jupyter Lab
edit: requirements-poetry requirements requirements-plotly
	poetry run jupyter lab --no-browser --ip 0.0.0.0

## Test python environment is setup correctly
test-environment: requirements requirements-plotly
	$(PYTHON_INTERPRETER) test_environment.py

## Download competition dataset
data: requirements data/raw/train.csv

## Clean competition dataset
clean-data: requirements data/processed/train.gz.parquet data/processed/test.gz.parquet

data/processed/train.gz.parquet: data/raw/train.csv
	poetry run python -m src.data.clean --input-file data/raw/train.csv --output-file data/processed/train.gz.parquet

data/processed/test.gz.parquet: data/raw/example_test.csv
	poetry run python -m src.data.clean --input-file data/raw/example_test.csv --output-file data/processed/test.gz.parquet

data/raw/%.csv: data/raw/mlb-player-digital-engagement-forecasting.zip
	unzip -f -d data/raw/ data/raw/mlb-player-digital-engagement-forecasting.zip
	if [ -e data/raw/$*.csv ]; then touch data/raw/$*.csv; fi

data/raw/mlb-player-digital-engagement-forecasting.zip:
	poetry run kaggle competitions download --path data/raw/mlb-player-digital-engagement-forecasting/ mlb-player-digital-engagement-forecasting
	touch data/raw/mlb-player-digital-engagement-forecasting.zip

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## List all kaggle kernels
kernels-list: requirements
	poetry run kaggle kernels list --user $(KAGGLE_USER) --competition $(KAGGLE_COMPETITION)

## Push all kernels that are out of date
kernels-push: requirements $(PUSH_KERNELS)

.make/kaggle/%: kaggle/%/*
	poetry run kaggle kernels push --path kaggle/$*
	mkdir -p $(dir $@)
	touch $@

## Install Python Dependencies
requirements: .make/requirements

.make/requirements: pyproject.toml poetry.lock .make/poetry
	poetry install
	if [ ! -e .make ]; then mkdir .make; fi
	touch .make/requirements

poetry.lock:

## Install plotly dependencies, requires node
requirements-plotly: .make/jupyter-lab

## Do not install plotly dependencies, ignore them
skip-plotly:
	touch .make/jupyter-lab

.make/jupyter-lab:
	poetry run jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyterlab-plotly plotlywidget
	touch .make/jupyter-lab

## Install poetry if not available
requirements-poetry: .make/poetry

.make/poetry:
ifeq (, $(shell which poetry))
ifeq (, $(shell which pipx))
	@echo Poetry not found, installing pipx first
	PIP_REQUIRE_VIRTUALENV= pip3 install --user pipx
endif
	@echo Installing Poetry in pipx container
	pipx install poetry
endif
	touch .make/poetry

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
