kaggle-baseball
===============

Submission for the MLB Player Digital Engagement Forecasting competition

https://www.kaggle.com/c/mlb-player-digital-engagement-forecasting

Project Requirements
--------------------

### Node Installation and Usage

This project uses Node to compile some extensions for Jupyter Lab.
If you do not wish to use plotly then you can skip this completely with `make skip-plotly`.
Installing node on your system can be done with:

```
brew install node
```

or

```
sudo apt-get install nodejs npm
```

Depending on which OS you use.

### Pyenv Installation and Usage

This project requires Python 3.9.
I recommend using [pyenv](https://github.com/pyenv/pyenv) to manage installations of python.
The makefile will not install pyenv - follow the [installation instructions](https://github.com/pyenv/pyenv#installation).

### Poetry Installation and Usage

This uses [poetry](https://poetry.eustace.io/docs/) as the package manager.
This will automatically install poetry using pipx if it is not available.

#### Installing Poetry Manually

You can install it by following the instructions [here](https://poetry.eustace.io/docs/#installation).
I recommend using [pipx](https://pipxproject.github.io/pipx/) to install it, which requires installing pipx:

```bash
pip install --user pipx
pipx install poetry
```

### Testing Project Setup

Poetry uses automatically managed virtual environments.
This project already has a pyproject.toml file so to create and test your virtual environment run:

```bash
make test-environment
```

This should print "Development environment passes all tests!".
This does use node to

### Adding Dependencies

You can install new packages with the command:

```bash
poetry add DEPENDENCY
```

This will record the changes in the pyproject.toml and poetry.lock files, so remember to commit them.

You can run commands directly in the virtual environment without activating it.
This is done by adding the `poetry run` prefix to your command, for example:

```bash
poetry run python -c 'print("Hello, World!")'
```

Invoking commands in this way means you will not change the virtual environment accidentally.

Project Organization
--------------------

    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── kaggle             <- Kaggle kernels for your submission
    │
    ├── pyproject.toml     <- The requirements file for reproducing the analysis environment
    │   poetry.lock
    │
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module
        │
        ├── data           <- Scripts to download or generate data
        │   └── make_dataset.py
        │
        ├── features       <- Scripts to turn raw data into features for modeling
        │   └── build_features.py
        │
        ├── models         <- Scripts to train models and then use trained models to make
        │   │                 predictions
        │   ├── predict_model.py
        │   └── train_model.py
        │
        └── visualization  <- Scripts to create exploratory and results oriented visualizations
            └── visualize.py

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
<p><small>Refined to use poetry as the package manager</small></p>
