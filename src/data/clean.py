"""
This cleans the csv files that are used for train and test.
Cleaning involves parsing the date and unpacking the json objects.
The clean data is written as a parquet file, as that can correctly encode the data.
"""
# pylint: disable=comparison-with-itself

import json
from pathlib import Path
from typing import Any, Union

import pandas as pd
import typer


def main(
    input_file: Path = typer.Option(
        ...,
        exists=True,
        file_okay=True,
        dir_okay=False,
        readable=True,
        help="CSV file to clean",
    ),
    output_file: Path = typer.Option(..., exists=False, help="Parquet file to create"),
) -> None:
    """This cleans a CSV file, parsing the date and unpacking the json objects.
    The clean data is written to a parquet file, which is a data format that
    can hold such data."""
    df = pd.read_csv(input_file)

    df["date"] = pd.to_datetime(df.date, format="%Y%m%d")
    for column in df.columns:
        if column == "date":
            continue
        df[column] = df[column].apply(unpack_json)

    df.to_parquet(output_file, compression="gzip")


def unpack_json(data: Union[float, str]) -> Any:
    """This unpacks the json objects in the CSV file.
    Missing data is represented as NaN (which is a float)."""
    if data != data:  # this is how you check for NaN
        return data
    return json.loads(data)


if __name__ == "__main__":
    typer.run(main)
