""" Common paths used by the local code.
    For kaggle kernels define the paths in the kernel.
    Remember that the kernel paths depend on the loaded dependencies. """

from pathlib import Path

PROJECT_ROOT = Path(__file__).resolve().parents[1]
DATA_FOLDER = PROJECT_ROOT / "data"
DATA_RAW_FOLDER = DATA_FOLDER / "raw"
DATA_PROCESSED_FOLDER = DATA_FOLDER / "processed"

DATA_CSV_TRAIN = DATA_RAW_FOLDER / "train.csv"
DATA_CSV_TEST = DATA_RAW_FOLDER / "example_test.csv"
DATA_CSV_SAMPLE_SUBMISSION = DATA_RAW_FOLDER / "example_sample_submission.csv"
DATA_CSV_AWARDS = DATA_RAW_FOLDER / "awards.csv"
DATA_CSV_PLAYERS = DATA_RAW_FOLDER / "players.csv"
DATA_CSV_SEASONS = DATA_RAW_FOLDER / "seasons.csv"
DATA_CSV_TEAMS = DATA_RAW_FOLDER / "teams.csv"

DATA_PARQUET_TRAIN = DATA_PROCESSED_FOLDER / "train.gz.parquet"
DATA_PARQUET_TEST = DATA_PROCESSED_FOLDER / "test.gz.parquet"
